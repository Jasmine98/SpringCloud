package cn.lingnan.controller;

import cn.lingnan.service.ClazzFeignService;
import cn.lingnan.service.GradeFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("grade/feign")
public class GradeConsumerFeignController {
    @Autowired
    private GradeFeignService gradeFeignService;

    @RequestMapping("queryAll")
    public Object queryAll() {
        return gradeFeignService.queryAll();
    }
}
