package cn.lingnan.dao;

import cn.lingnan.entity.Grade;
import cn.lingnan.entity.LoginForm;
import cn.lingnan.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface StudentDao {
    //验证登录信息
    Student login(LoginForm loginform);
    //通过学号查询指定学生信息
    Student findBySno(Student student);
    //根据姓名查询指定/所有学生信息列表
    List<Student> selectList(Student student);
    //插入学生信息
    int insert(Student student);
    //根据id学生信息
    int update(Student student);
    //根据id修改指定学生密码
    int updatePassword(Student student);
    //根据id删除指定信息
    int deleteById(Integer[] ids);
    List<Student> queryAll();
    Student queryById(Integer id);

}
