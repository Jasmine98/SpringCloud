package cn.lingnan.controller;

import cn.lingnan.service.AdminFeignService;
import cn.lingnan.service.TeacherFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("teacher/feign")
public class TeacherConsumerFeignController {
    @Autowired
    private TeacherFeignService teacherFeignService;

    @RequestMapping("queryAll")
    public Object queryAll() {
        return teacherFeignService.queryAll();
    }
}
