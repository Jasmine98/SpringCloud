package cn.lingnan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Clazz {
    //班级信息
    private Integer id;
    private String name;
    private String number;

    //班主任信息
    private String coordinator;
    private String telephone;
    private String email;
    //所属年级
    private String grade_name;

    public Clazz(String clazzname, String gradename) {
    }
}
