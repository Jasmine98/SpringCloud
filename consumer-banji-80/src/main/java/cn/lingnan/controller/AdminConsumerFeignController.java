package cn.lingnan.controller;

import cn.lingnan.service.AdminFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("admin/feign")
public class AdminConsumerFeignController {
    @Autowired
    private AdminFeignService adminFeignService;

    @RequestMapping("queryAll")
    public Object queryAll() {
        return adminFeignService.queryAll();
    }
}
