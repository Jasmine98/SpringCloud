package cn.lingnan.service.impl;

import cn.lingnan.dao.ClazzDao;
import cn.lingnan.entity.Admin;
import cn.lingnan.entity.Clazz;
import cn.lingnan.service.ClazzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClazzServiceImpl implements ClazzService {
    @Autowired
    private ClazzDao clazzDao;

    @Override
    public List<Clazz> selectList(Clazz clazz) {
        return clazzDao.selectList(clazz);
    }

    @Override
    public List<Clazz> selectAll() {
        return clazzDao.selectAll();
    }

    @Override
    public int insert(Clazz clazz) {
        return clazzDao.insert(clazz);
    }

    @Override
    public int deleteById(Integer[] ids) {
        return clazzDao.deleteById(ids);
    }

    @Override
    public Clazz findByName(String name) {
        return clazzDao.findByName(name);
    }

    @Override
    public int update(Clazz clazz) {
        return clazzDao.update(clazz);
    }


    @Override
    public List<Clazz> queryAll() {
        return clazzDao.queryAll();
    }

    @Override
    public Clazz queryById(Integer id) {
        return clazzDao.queryById(id);
    }

}
