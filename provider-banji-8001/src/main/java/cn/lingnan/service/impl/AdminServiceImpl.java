package cn.lingnan.service.impl;


import cn.lingnan.dao.AdminDao;
import cn.lingnan.entity.Admin;
import cn.lingnan.entity.LoginForm;
import cn.lingnan.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminDao adminDao;


    @Override
    public Admin login(LoginForm loginForm) {
        return adminDao.login(loginForm);
    }

    @Override
    public Admin findByName(String name) {
        return adminDao.findByName(name);
    }

    @Override
    public int insert(Admin admin) {
        return adminDao.insert(admin);
    }

    @Override
    public List<Admin> selectList(Admin admin) {
        return adminDao.selectList(admin);
    }

    @Override
    public int update(Admin admin) {
        return adminDao.update(admin);
    }

    @Override
    public int updatePassword(Admin admin) {
        return adminDao.updatePassword(admin);
    }

    @Override
    public int deleteById(Integer[] ids) {
        return adminDao.deleteById(ids);
    }



    @Override
    public List<Admin> queryAll() {
        return adminDao.queryAll();
    }

    @Override
    public Admin queryById(Integer id) {
        return adminDao.queryById(id);
    }


}
