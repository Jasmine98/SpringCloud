package cn.lingnan.service.impl;

import cn.lingnan.dao.StudentDao;
import cn.lingnan.entity.LoginForm;
import cn.lingnan.entity.Student;
import cn.lingnan.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//业务层-操控学生信息
@Service

public class StudentServiceImpl implements StudentService {
    //注入Mapper接口对象
    @Autowired
    private StudentDao studentDao;

    @Override
    public Student login(LoginForm loginForm) {

        return studentDao.login(loginForm);
    }

    @Override
    public List<Student> selectList(Student student) {

        return studentDao.selectList(student);
    }

    @Override
    public Student findBySno(Student student) {

        return studentDao.findBySno(student);
    }

    @Override
    public int insert(Student student) {

        return studentDao.insert(student);
    }

    @Override
    public int update(Student student) {
        return studentDao.update(student);
    }

    @Override
    public int updatePassword(Student student) {
        return studentDao.updatePassword(student);
    }

    @Override
    public int deleteById(Integer[] ids) {
        return studentDao.deleteById(ids);
    }

    @Override
    public List<Student> queryAll() {
        return studentDao.queryAll();
    }

    @Override
    public Student queryById(Integer id) {
        return studentDao.queryById(id);
    }
}
