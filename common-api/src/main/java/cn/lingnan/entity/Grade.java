package cn.lingnan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Grade {
    //年级信息
    private Integer id;
    private String name;
    //年级主任信息
    private String manager;
    private String email;
    private String telephone;


}
