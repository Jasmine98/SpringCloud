package cn.lingnan.service.impl;

import cn.lingnan.dao.TeacherDao;
import cn.lingnan.entity.LoginForm;
import cn.lingnan.entity.Teacher;
import cn.lingnan.service.TeacherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

//业务层实现类-操控教师信息
@Service

public class TeacherServiceImpl implements TeacherService {
    //注入Mapper接口对象
    @Autowired
    private TeacherDao teacherDao;

    @Override
    public Teacher login(LoginForm loginForm) {
        return teacherDao.login(loginForm);
    }

    @Override
    public List<Teacher> selectList(Teacher teacher) {
        return teacherDao.selectList(teacher);
    }

    @Override
    public Teacher findByTno(Teacher teacher) {
        return teacherDao.findByTno(teacher);
    }

    @Override
    public int insert(Teacher teacher) {
        return teacherDao.insert(teacher);
    }

    @Override
    public int update(Teacher teacher) {
        return teacherDao.update(teacher);
    }

    @Override
    public int deleteById(Integer[] ids) {
        return teacherDao.deleteById(ids);
    }

    @Override
    public int updatePassword(Teacher teacher) {
        return teacherDao.updatePassword(teacher);
    }

    @Override
    public List<Teacher> queryAll() {
        return teacherDao.queryAll();
    }

    @Override
    public Teacher queryById(Integer id) {
        return teacherDao.queryById(id);
    }
}
