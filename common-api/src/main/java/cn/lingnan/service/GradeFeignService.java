package cn.lingnan.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(value = "provider-grade")
public interface GradeFeignService {
    @GetMapping("/grade/queryAll")
    Object queryAll();
}
