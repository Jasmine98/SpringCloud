package cn.lingnan.controller;


import cn.lingnan.entity.Clazz;
import cn.lingnan.entity.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("grade/grade")
public class GradeConsumerController {
    @Autowired
    private RestTemplate restTemplate;
    private String url_prefix = "http://provider-grade";//"http://localhost:8001";

    @RequestMapping("insert")
    public ResponseEntity<Boolean> insert(Grade bean) {
        String uri = url_prefix + "/grade/insert";
        return restTemplate.postForEntity(uri, bean, Boolean.class);
    }

    @RequestMapping("queryAll")
    public Object queryAll() {
        String uri = url_prefix + "/grade/queryAll";
        return restTemplate.getForObject(uri, List.class);
    }

    @RequestMapping("queryById/{id}")
    public Object queryById(@PathVariable Integer id) {
        String uri = url_prefix + "/grade/queryById/" + id;
        return restTemplate.getForObject(uri, Grade.class);
    }


}
