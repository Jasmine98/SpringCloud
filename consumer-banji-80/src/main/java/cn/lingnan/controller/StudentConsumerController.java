package cn.lingnan.controller;


import cn.lingnan.entity.Admin;
import cn.lingnan.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("student/consumer")
public class StudentConsumerController {
    @Autowired
    private RestTemplate restTemplate;
    private String url_prefix = "http://provider-student";//"http://localhost:8001";



    @RequestMapping("insert")
    public ResponseEntity<Boolean> insert(Student bean) {
        String uri = url_prefix + "/student/insert";
        return restTemplate.postForEntity(uri, bean, Boolean.class);
    }

    @RequestMapping("queryAll")
    public Object queryAll() {
        String uri = url_prefix + "/student/queryAll";
        return restTemplate.getForObject(uri, List.class);
    }

    @RequestMapping("queryById/{id}")
    public Object queryById(@PathVariable Integer id) {
        String uri = url_prefix + "/student/queryById/" + id;
        return restTemplate.getForObject(uri, Student.class);
    }
}
