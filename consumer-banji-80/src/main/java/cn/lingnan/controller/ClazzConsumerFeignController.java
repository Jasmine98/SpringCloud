package cn.lingnan.controller;

import cn.lingnan.service.AdminFeignService;
import cn.lingnan.service.ClazzFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("clazz/feign")
public class ClazzConsumerFeignController {
    @Autowired
    private ClazzFeignService clazzFeignService;

    @RequestMapping("queryAll")
    public Object queryAll() {
        return clazzFeignService.queryAll();
    }
}
