package cn.lingnan.controller;

import cn.lingnan.service.AdminFeignService;
import cn.lingnan.service.StudentFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("student/feign")
public class StudentConsumerFeignController {
    @Autowired
    private StudentFeignService studentFeignService;

    @RequestMapping("queryAll")
    public Object queryAll() {
        return studentFeignService.queryAll();
    }
}
