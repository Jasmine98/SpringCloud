package cn.lingnan.dao;

import cn.lingnan.entity.Admin;
import cn.lingnan.entity.LoginForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminDao {
    //验证登录信息
    Admin login(LoginForm loginForm);
    //通过姓名查询指定管理员信息
    Admin findByName(String name);
    //添加管理员信息
    int insert(Admin admin);
    //根据姓名查询指定/所有管理员信息列表
    List<Admin> selectList(Admin admin);
    //根据id更新指定管理员信息
    int update(Admin admin);
    //根据id修改指定管理员密码
    int updatePassword(Admin admin);
    //根据id删除指定管理员信息
    int deleteById(Integer[] ids);
    List<Admin> queryAll();
    Admin queryById(Integer id);
}

