package cn.lingnan.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(value = "provider-admin")
public interface AdminFeignService {
    @GetMapping("/admin/queryAll")
    Object queryAll();
}
