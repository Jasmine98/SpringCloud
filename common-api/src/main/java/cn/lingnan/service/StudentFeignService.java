package cn.lingnan.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(value = "provider-student")
public interface StudentFeignService {
    @GetMapping("/student/queryAll")
    Object queryAll();
}
