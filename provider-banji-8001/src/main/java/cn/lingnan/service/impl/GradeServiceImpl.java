package cn.lingnan.service.impl;

import cn.lingnan.dao.ClazzDao;
import cn.lingnan.dao.GradeDao;
import cn.lingnan.entity.Clazz;
import cn.lingnan.entity.Grade;
import cn.lingnan.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

//业务层实现类-操控年级信息
    @Service
    public class GradeServiceImpl implements GradeService {
        //注入Mapper接口对象
        @Autowired
        private GradeDao gradeDao;


    @Override
    public List<Grade> selectList(Grade gradename) {
        return gradeDao.selectList(gradename);
    }

    @Override
    public List<Grade> selectAll() {
        return gradeDao.selectAll();
    }

    @Override
    public Grade findByName(String gradename) {
        return gradeDao.findByName(gradename);
    }

    @Override
    public int insert(Grade grade) {
        return gradeDao.insert(grade);
    }

    @Override
    public int update(Grade grade) {
        return gradeDao.update(grade);
    }

    @Override
    public int deleteById(Integer[] ids) {
        return gradeDao.deleteById(ids);
    }
    @Override
    public List<Grade> queryAll() {
        return gradeDao.queryAll();
    }

    @Override
    public Grade queryById(Integer id) {
        return gradeDao.queryById(id);
    }
}
